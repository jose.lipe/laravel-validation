<?php

namespace App\Providers;

use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Code\Validator\Cnpj;
use Code\Validator\Cpf;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('documento', function ($attribute, $value, $parameters, $validator){
            if ($parameters[0] == 'cpf') {
                $documentValidator = new Cpf();
            }else {
                $documentValidator = new Cnpj();
            }

            return $documentValidator->isValid($value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(IdeHelperServiceProvider::class);
            $this->app->singleton(Generator::class, function () {
                return Factory::create(env('FAKER_LANGUAGE'));
            });
        }
    }
}
